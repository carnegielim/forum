package forum.controllers;

import forum.service.AdminService;
import forum.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.concurrent.atomic.AtomicLong;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private MainService mainService;

    @RequestMapping("/delresponse")
    public String delresponse(@RequestParam(value="id")Long responseId) {
        long postid = mainService.getPostByResponseId(responseId);
        adminService.delResponse(postid,responseId);
        return "redirect:/post?id="+postid;
    }
    @RequestMapping("/delpost")
    public String delpost(@RequestParam(value="id")Long postid,@RequestParam(value="block")Long block) {
        adminService.delPost(postid);
        return "redirect:/allblock/block?block="+block;
    }
    @RequestMapping("/delpostbyresponse")
    public String delPostByResponse(@RequestParam(value="id")Long response_id) {
        long postid = mainService.getPostByResponseId(response_id);
        long block = mainService.getBelongToBlockById(postid);
        adminService.delPost(postid);

        return "redirect:/allblock/block?block="+ block;
    }
}