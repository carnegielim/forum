package forum.controllers;


import forum.entity.Block;
import forum.entity.Response;
import forum.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller

public class IndexController {
    @Autowired
    private MainService mainService;
    @RequestMapping("/")
    public String index(Model model) {
        List<Block> popularBlock= mainService.getPopularBlock();
        model.addAttribute("block1",popularBlock.get(0));
        model.addAttribute("block2",popularBlock.get(1));
        model.addAttribute("block3",popularBlock.get(2));
        model.addAttribute("block4",popularBlock.get(3));
        model.addAttribute("postNum",mainService.getPostNum());
        model.addAttribute("blockNum",mainService.getBlockNum());
        model.addAttribute("userNum",mainService.getUserNum());
        model.addAttribute("responseNum",mainService.getResponseNum());
        model.addAttribute("lastUser",mainService.getLastUsers());
        model.addAttribute("monthNum",mainService.getNumOfMonth());
        return "index";
    }
    @RequestMapping("/allblock")
    public String allblock(Model model) {
        List<Block> lb = mainService.getParentBlocks();
        model.addAttribute("block1",lb.get(0));
        model.addAttribute("block2",lb.get(1));
        model.addAttribute("block3",lb.get(2));
        model.addAttribute("block4",lb.get(3));
        return "allblock";
    }
    @RequestMapping("/allblock/childblock")
    public String childblock(Model model,
                             @RequestParam(value="block", defaultValue="1")Long block,
                             @RequestParam(value="pageIndex", defaultValue="1")Long pageIndex){


        String parent = mainService.getBlockNameById(block);
        model.addAttribute("parent",parent);
        model.addAttribute("parentid",block);
        model.addAttribute("total",mainService.countByParentBlock(block));
        model.addAttribute("pageIndex",pageIndex);

        return "childblock";
    }
    @RequestMapping("/allblock/block")
    public String block(Model model,
                             @RequestParam(value="block", defaultValue="5")Long block,
                             @RequestParam(value="pageIndex", defaultValue="1")Long pageIndex){

        String parent = mainService.getBlockNameById(block);
        model.addAttribute("parent",parent);
        model.addAttribute("parentid",block);
        model.addAttribute("total",mainService.countPost(block));
        model.addAttribute("pageIndex",pageIndex);

        return "block";
    }
    @RequestMapping("/login")
    public String login(Model model){

        return "login";
    }
    @RequestMapping("/tologout")
    public String tologout(Model model,HttpSession session){
        session.removeAttribute("user");
        session.removeAttribute("userid");
        session.removeAttribute("headimg");
        session.removeAttribute("time");
        session.removeAttribute("isAdmin");
        return "redirect:/login";
    }
    @RequestMapping(value="/register" ,method={RequestMethod.POST})
    public String register(Model model,@RequestParam(value="username")String name,@RequestParam(value="password")String passwd){
        String success = null,failed = null;
        try{
            mainService.register(name,"headimg/nohead.png",passwd);
            model.addAttribute("success","button");
        }catch (Exception e){
            model.addAttribute("failed","button");
        }
        return "registertip";
    }
    @RequestMapping(value="/tologin" ,method={RequestMethod.POST})
    public String tologin(Model model, HttpSession session,
                          @RequestParam(value="username")String name, @RequestParam(value="password")String passwd){
        if(mainService.login(name,passwd,session)){
            return "redirect:/";
        }
        return "logintip";
    }
    @RequestMapping(value="/post")
    public String post(Model model,@RequestParam(value="id")Long post_id,
                           @RequestParam(value="pageIndex", defaultValue="1")Long pageIndex){
        String postname = mainService.getPostNameById(post_id);
        model.addAttribute("postname",postname);
        model.addAttribute("postid",post_id);
        model.addAttribute("total",mainService.countResponse(post_id));
        model.addAttribute("pageIndex",pageIndex);

        return "post";
    }
    @RequestMapping(value="/submit")
    public String submit(@RequestParam(value="content")String content, @RequestParam(value="postid")Long postid, HttpSession session){
        mainService.respond(postid,(Long) session.getAttribute("userid"),content);
        return "redirect:/post?id="+postid;
    }
    @RequestMapping(value="/topost")
    public String topost(@RequestParam(value="title")String title,@RequestParam(value="content")String content,
                         @RequestParam(value="blockid")Long blockid, HttpSession session){
        mainService.posting(title,blockid,(Long)session.getAttribute("userid"),content);
        return "redirect:/allblock/block?block="+blockid;
    }

}
