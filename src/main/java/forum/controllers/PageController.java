package forum.controllers;

import forum.entity.Block;
import forum.entity.Post;
import forum.entity.Response;
import forum.jpa.PostJPA;
import forum.service.MainService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("/page")
public class PageController {
    @Autowired
    private MainService mainService;
    @Autowired
    PostJPA postJPA;
    @RequestMapping("/block")
    public JSONArray block(@RequestParam(value="block", defaultValue="1")Long block,
                            @RequestParam(value="pageIndex", defaultValue="1")Long pageIndex) {
        List<Block> lb = mainService.getChildBlocks(block,pageIndex,(long)4);

        JSONArray jsonArray = new JSONArray();
        for(Block block1:lb){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",block1.getName());
            jsonObject.put("introduction",block1.getIntroduction());
            jsonObject.put("id",block1.getId());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }
    @RequestMapping("/post")
    public JSONArray post(@RequestParam(value="block", defaultValue="1")Long block,
                           @RequestParam(value="pageIndex", defaultValue="1")Long pageIndex) {
        List<Post> lb = mainService.getPosts(block,pageIndex,(long)5);

        JSONArray jsonArray = new JSONArray();
        for(Post post:lb){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",post.getId());
            jsonObject.put("name",post.getName());
            jsonObject.put("author",post.getAuthor());
            jsonObject.put("short",post.getShortView());
            jsonObject.put("last",post.getLastResponseTime());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }
    @RequestMapping("/response")
    public JSONArray response(@RequestParam(value="post", defaultValue="1")Long post,
                              @RequestParam(value="pageIndex", defaultValue="1")Long pageIndex) {
        List<Response> lr = mainService.getResponse(post,pageIndex,(long)5);

        JSONArray jsonArray = new JSONArray();
        for(Response response:lr){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",response.getId());
            jsonObject.put("author",mainService.getUserNameById(response.getAuthor()));
            jsonObject.put("content",response.getContent());
            jsonObject.put("time",response.getResponseTime());
            jsonObject.put("rank",response.getFloor());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }
    @RequestMapping("/test")
    public Long test() {
        java.math.BigInteger a = (BigInteger)postJPA.getPopularBlock().get(0)[1];
        return a.longValue();
    }
}
