package forum.controllers;

import forum.entity.Block;
import forum.entity.Post;
import forum.entity.Response;
import forum.entity.User;
import forum.jpa.PostJPA;
import forum.jpa.ResponseJPA;
import forum.jpa.UserJPA;
import forum.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private BlockService blockService;
    @Autowired
    private PostService postService;
    @Autowired
    private MainService mainService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private UserJPA userJPA;



    @RequestMapping(value = "/month",method = RequestMethod.GET)
    public BigInteger getchild(@RequestParam(value="month", defaultValue="1")int month){
        return userJPA.getNumOfMonth(1);
    }

//    (Long id,String name,Long belongToblock,String lastResponseTime,Long author,String shortView)
    @RequestMapping(value = "/void",method = RequestMethod.GET)
    public void getchild(){
//        mainService.updatePassword((long)1,"123456");
//        userJPA.updatePassword((long)1,"1234561");
    }
}
