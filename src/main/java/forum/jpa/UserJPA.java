package forum.jpa;

import forum.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@Component
public interface UserJPA extends JpaRepository<User,Long>{




    @Query(value="SELECT password from users WHERE name=:name",nativeQuery=true)
    String findPasswordByName(@Param("name")String name);

    @Modifying
    @Transactional
    @Query(value="UPDATE users SET name= :name WHERE id=:id",nativeQuery=true)
    void updateName(@Param("id")Long id,@Param("name")String name);

    @Modifying
    @Transactional
    @Query(value="UPDATE users SET head= :head WHERE id=:id",nativeQuery=true)
    void updateHead(@Param("id")Long id,@Param("head")String head);

    @Modifying
    @Transactional
    @Query(value="UPDATE users SET email= :email WHERE id=:id",nativeQuery=true)
    void updateEmail(@Param("id")Long id,@Param("email")String email);

    @Modifying
    @Transactional
    @Query(value="UPDATE users SET password= md5(:password) WHERE id=:id",nativeQuery=true)
    void updatePassword(@Param("id")Long id,@Param("password")String password);

    long count();

    @Query(value="SELECT name,head,regis from users order by id DESC limit 8",nativeQuery=true)
    List<Object[]> findLastUsers();

    User findAllByName(String name);
    @Query(value="SELECT id from users where name = :name",nativeQuery=true)
    Long findIdbyName(@Param("name")String name);

    @Query(value="SELECT name from users where id = :id",nativeQuery=true)
    String findNamebyId(@Param("id")Long id);

    @Query(value="SELECT count(id) from users where month (regis) = :month group by month (regis)",nativeQuery=true)
    BigInteger getNumOfMonth(@Param("month")int month);
}

