package forum.jpa;

import forum.entity.Block;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Component
public interface BlockJPA extends JpaRepository<Block,Long>{
    /**
     * 根据父板块id得到子版块
     * @param parentBlock 父板块id
     * @return 所有响应的子版块
     */
    List<Block> findByParentBlock(Long parentBlock);

    /**
     * 按页得到子版块
     * @param parentBlock 父板块id
     * @param startIndex  页号开始的位置
     * @param pageSize 页大小
     * @return 该页所有的子版块
     */
    @Query(value="select * from blocks where parent_block = :parentBlock limit :startIndex, :pageSize ",nativeQuery=true)
    List<Block> getChildBlockByPage(@Param("parentBlock")Long parentBlock, @Param("startIndex")Long startIndex, @Param("pageSize")Long pageSize);

    /**
     *
     * @return 所有申请创建中的板块
     */
    @Query(value="select name,introduction,headimg,parent_block from blocks where parent_block < 0",nativeQuery=true)
    List<Object> getAllNewBlock();



    long count();

    @Query(value="SELECT name from blocks where id=:id",nativeQuery=true)
    String getNameById(@Param("id")Long id);
    Long countByParentBlock(Long id);

    @Query(value="SELECT name,headimg from blocks where id=:id",nativeQuery=true)
    String[] getNameAndHeadById(@Param("id")Long id);


}
