package forum.jpa;

import forum.entity.Response;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Component
public interface ResponseJPA extends JpaRepository<Response,Long>{

    @Query(value="select * from responses where belong_to_post = :belongToPost order by response_time limit :startIndex, :pageSize ",nativeQuery=true)
    List<Response> getResponseByPage(@Param("belongToPost")long belongToPost,@Param("startIndex")long startIndex,@Param("pageSize")long pageSize);


    @Transactional
    Long deleteAllByBelongToPost(Long belongToPost);

    @Query(value="select response_time from responses where belong_to_post = :belongToPost order by response_time desc limit 1",nativeQuery=true)
    String getLastResponseTime(@Param("belongToPost")Long belongToPost);

    long count();

    @Query(value="select floor from responses where belong_to_post = :belongToPost order by id desc limit 1",nativeQuery=true)
    Long getLastFloorByBelongToPost(@Param("belongToPost")Long belongToPost);

    long countByBelongToPost(long id);

    @Query(value="select belong_to_post from responses where id = :id",nativeQuery=true)
    Long getPostByResponseId(@Param("id")Long id);


}