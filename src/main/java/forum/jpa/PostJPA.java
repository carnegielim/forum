package forum.jpa;

import forum.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Component
public interface PostJPA extends JpaRepository<Post,Long>{
    @Query(value="select * from posts where belong_to_block = :belongToBlock order by last_response_time DESC limit :startIndex, :pageSize ",nativeQuery=true)
    List<Post> getPostByPage(@Param("belongToBlock")long belongToBlock,@Param("startIndex")long startIndex,@Param("pageSize")long pageSize);


    @Modifying
    @Transactional
    @Query(value="UPDATE posts SET last_response_time=:newTime WHERE id=:id",nativeQuery=true)
    void updataLastResponseTime(@Param("id")Long id,@Param("newTime")String newTime);

    long count();

    long countByBelongToblock(long belongToBlock);

    @Query(value="SELECT name from posts where id=:id",nativeQuery=true)
    String getNameById(@Param("id")Long id);

    @Query(value="SELECT belong_to_block from posts where id=:id",nativeQuery=true)
    long getBelongToBlockById(@Param("id")Long id);

    @Query(value="SELECT belong_to_block,count(id) from posts group by belong_to_block order by count(id) DESC limit 4",nativeQuery=true)
    List<Object[]> getPopularBlock();
}