package forum.service;

import forum.entity.Post;
import forum.jpa.PostJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class PostService {
    @Autowired
    PostJPA postJPA;

    public List<Post> findAll(){
        return postJPA.findAll();
    }
    public List<Post> getPosts(int belongToBlock,int pageNo,int pageSize){
        return postJPA.getPostByPage(belongToBlock,(pageNo-1)*pageSize,pageSize);
    }
    public void posting(String name,Long belongToblock,Long author,String shortView){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        postJPA.save(new Post(name,belongToblock,time,author,shortView));
    }
    public void delPost(int id){
        postJPA.deleteById(new Long(id));
    }

}
