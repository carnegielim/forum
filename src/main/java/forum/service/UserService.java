package forum.service;

import forum.entity.User;
import forum.jpa.UserJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserJPA userJPA;

    public List<User> findAll(){
        return userJPA.findAll();
    }

}
