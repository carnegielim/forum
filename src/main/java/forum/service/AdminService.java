package forum.service;


import forum.jpa.BlockJPA;
import forum.jpa.PostJPA;
import forum.jpa.ResponseJPA;
import forum.jpa.UserJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
    @Autowired
    PostJPA postJPA;
    @Autowired
    ResponseJPA responseJPA;
    @Autowired
    BlockJPA blockJPA;
    @Autowired
    UserJPA userJPA;

    public void delResponse(Long postId,Long responseId){
        responseJPA.deleteById(responseId);
        String newTime = responseJPA.getLastResponseTime(postId);
        postJPA.updataLastResponseTime(postId,newTime);
    }
    public void delPost(Long id){
        responseJPA.deleteAllByBelongToPost(id);
        postJPA.deleteById(id);

    }


    public List<Object> getNewBlock(){
        return blockJPA.getAllNewBlock();
    }
}
