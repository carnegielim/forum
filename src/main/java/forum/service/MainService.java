package forum.service;

import forum.entity.Block;
import forum.entity.Post;
import forum.entity.Response;

import forum.entity.User;
import forum.jpa.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpSession;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MainService {
    @Autowired
    PostJPA postJPA;
    @Autowired
    ResponseJPA responseJPA;
    @Autowired
    BlockJPA blockJPA;
    @Autowired
    UserJPA userJPA;
    @Autowired
    AdminJPA adminJPA;

    public void posting(String postName,Long belongToblock,Long author,String content){
        String shortView = null;
        if(content.length()>10)
            shortView = content.substring(0,7)+"...";
        else
            shortView = content;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        Post p = new Post(postName,belongToblock,time,author,shortView);
        postJPA.save(p);
        responseJPA.save(new Response(p.getId(),author,time,(long)1,content));
    }

    public long getPostByResponseId(Long id){
        return responseJPA.getPostByResponseId(id);
    }
    public long getBelongToBlockById(long id){return postJPA.getBelongToBlockById(id);}
    public void respond(Long belongToPost,Long author,String content){
        content = content.replaceAll("&","&amp;");
        content = content.replaceAll("<","&lt;");
        content = content.replaceAll(">","&gt;");
        content = content.replaceAll("\"","&quot;");
        content = content.replaceAll("\'","&#39;");
        content = content.replaceAll("\\(","&#40;");
        content = content.replaceAll("\\)","&#41;");
        content = content.replaceAll("%","&#37;");
        content = content.replaceAll("\\+","&#43;");
        content = content.replaceAll("-","&#45;");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        Long floor = responseJPA.getLastFloorByBelongToPost(belongToPost)+1;
        Response r = new Response(belongToPost,author,time,floor,content);
        responseJPA.save(r);
        postJPA.updataLastResponseTime(r.getBelongToPost(),time);
    }
    public List<Post> getPosts(long belongToBlock, long pageNo, long pageSize){
        return postJPA.getPostByPage(belongToBlock,(pageNo-1)*pageSize,pageSize);
    }
    public List<Response> getResponse(long belongToPost,long pageNo,long pageSize){
        return responseJPA.getResponseByPage(belongToPost,(pageNo-1)*pageSize,pageSize);
    }
    public List<Block> getParentBlocks(){
        return blockJPA.findByParentBlock((long)0);
    }
    public List<Block> getChildBlocks(Long parentBlock,Long pageNo,Long pageSize){
        return blockJPA.getChildBlockByPage(parentBlock,(pageNo-1)*pageSize,pageSize);
    }
    public void register(String name,String head,String password){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        userJPA.save(new User(name,head,time,password));
    }
    public boolean isAdmin(Long id){
        if(adminJPA.findAllById(id)!=null)
            return true;
        return false;
    }
    public boolean login(String username, String password, HttpSession session){
        if(password == null)
            return false;
        User user = userJPA.findAllByName(username);
        String md5 = user.getPassword();
        try{
            if(md5.equals(DigestUtils.md5DigestAsHex(password.getBytes() ) ) ) {
                session.setAttribute("user",username);
                session.setAttribute("headimg",user.getHead());
                session.setAttribute("userid",user.getId());
                session.setAttribute("time",user.getRegis());
                session.setAttribute("isAdmin",this.isAdmin(userJPA.findIdbyName(username)));
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }
    public String getUserNameById(Long id){return userJPA.findNamebyId(id);}
    public void logout(Long id,HttpSession session){
        session.setAttribute("user",null);
    }
    public void applyForBlock(String name,String introduction,String headimg,Long parentBlock){
        blockJPA.save(new Block(name,introduction,headimg,-parentBlock));
    }
    public void updatePassword(Long id,String password){
        userJPA.updatePassword(id,DigestUtils.md5DigestAsHex(password.getBytes()));
    }
    public String getBlockNameById(Long id){return blockJPA.getNameById(id);}
    public List<Block> getPopularBlock(){
        List<Object[]> lo = postJPA.getPopularBlock();
        List<Integer> li = new ArrayList<Integer>();
        List<BigInteger> lb = new ArrayList<BigInteger>();
        for(Object[] objects:lo){
            li.add((Integer)objects[0]);
            lb.add((BigInteger)objects[1]);
        }
        List<Long> ll = new ArrayList<Long>();
        for(Integer integer:li){
            ll.add(integer.longValue());
        }
        List<Block> blocks = blockJPA.findAllById(ll);
        for(int i=0;i<4;i++){
            blocks.get(i).setId(lb.get(i).longValue());
        }
        return blocks;

    }
    public long getPostNum(){return postJPA.count();}
    public long getBlockNum(){return blockJPA.count();}
    public long getUserNum(){return userJPA.count();}
    public long getResponseNum(){return responseJPA.count();}
    public long countByParentBlock(long parent){return blockJPA.countByParentBlock(parent);}
    public long countPost(long id){return postJPA.countByBelongToblock(id);}
    public String getPostNameById(long id){return postJPA.getNameById(id);}
    public long countResponse(long id){return  responseJPA.countByBelongToPost(id);}
    public List<Object[]> getLastUsers(){return userJPA.findLastUsers();}
    public List<Long> getNumOfMonth(){
        List<Long> ll = new ArrayList<Long>();
        for(int i=1;i<=12;i++){
            BigInteger bigInteger = userJPA.getNumOfMonth(i);
            long a = bigInteger.longValue();
            ll.add(a);
        }
        return ll;
    }
}
