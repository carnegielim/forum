package forum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import forum.jpa.BlockJPA;
import forum.entity.Block;
import java.util.List;
import java.util.Optional;


@Service
public class BlockService {
    @Autowired
    BlockJPA blockJPA;

    public List<Block> findAllParentBlock(){
        return blockJPA.findByParentBlock((long)0);
    }
    public List<Block> findChildBlock(Long parent){
        return blockJPA.findByParentBlock(parent);
    }


}
