package forum.entity;

import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "head")
    private String head;
    @Column(name = "regis")
    private String regis;
    @Column(name = "password")
    @ColumnTransformer(
            write = "md5(?)"
    )
    private String password;

    public User(){}
    public User(String name,String head,String regis,String password){
        this.name = name;
        this.head = head;
        this.regis = regis;
        this.password = password;
    }
    public Long getId(){return this.id;}
    public String getName(){return this.name;}
    public String getHead(){return this.head;}
    public String getRegis(){return this.regis;}
    public String getPassword(){return  this.password;}


    public void setId(Long id){this.id = id;}
    public void setName(String name){this.name = name;}
    public void setHeadUrl(String head){this.head = head;}
    public void setRegisTime(String regis){this.regis = regis;}
    public void setPassword(String password){this.password = password;}
}
