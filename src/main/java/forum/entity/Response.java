package forum.entity;

import javax.persistence.*;

@Entity
@Table(name = "responses")
public class Response {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "belong_to_post")
    private Long belongToPost;
    @Column(name = "author")
    private Long author;
    @Column(name = "response_time")
    private String responseTime;
    @Column(name = "floor")
    private Long floor;
    @Column(name = "content")
    private String content;

    public Response(){}
    public Response(Long belongToPost,Long author,String responseTime,Long floor,String content){
        this.belongToPost = belongToPost;
        this.author = author;
        this.responseTime = responseTime;
        this.floor = floor;
        this.content = content;
    }
    public void setId(Long id){this.id = id;}
    public void setBelongToPost(Long belongToPost){this.belongToPost = belongToPost;}
    public void setAuthor(Long author){this.author = author;}
    public void setResponseTime(String responseTime){this.responseTime = responseTime;}
    public void setFloor(Long floor){this.floor = floor;}
    public void setContent(String content){this.content = content;}

    public Long getId(){return this.id;}
    public Long getBelongToPost(){return this.belongToPost;}
    public Long getAuthor(){return this.author;}
    public String getResponseTime(){return this.responseTime;}
    public Long getFloor(){return this.floor;}
    public String getContent(){return this.content;}
}
