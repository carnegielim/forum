package forum.entity;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "belong_to_block")
    private Long belongToblock;
    @Column(name = "last_response_time")
    private String lastResponseTime;
    @Column(name = "author")
    private Long author;
    @Column(name = "short_view")
    private String shortView;

    public Post(){}
    public Post(String name,Long belongToblock,String lastResponseTime,Long author,String shortView){
        this.name = name;
        this.belongToblock = belongToblock;
        this.lastResponseTime = lastResponseTime;
        this.author = author;
        this.shortView = shortView;
    }
    public void setId(Long id){this.id = id;}
    public void setName(String name){this.name = name;}
    public void setBelongToblock(Long belongToblock){this.belongToblock = belongToblock;}
    public void setLastResponseTime(String lastResponseTime){this.lastResponseTime = lastResponseTime;}
    public void setAuthor(Long author){this.author = author;}
    public void setShortView(String shortView){this.shortView = shortView;}

    public Long getId(){return this.id;}
    public String getName(){return this.name;}
    public Long getBelongToblock(){return this.belongToblock;}
    public String getLastResponseTime(){return this.lastResponseTime;}
    public Long getAuthor(){return this.author;}
    public String getShortView(){return this.shortView;}
}
