package forum.entity;

import javax.persistence.*;

@Entity
@Table(name = "blocks")
public class Block {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "introduction")
    private String introduction;
    @Column(name = "headimg")
    private String headimg;
    @Column(name = "parent_block")
    private Long parentBlock;

    public Block(){}
    public Block(String name,String introduction,String headimg,Long parentBlock){
        this.name = name;
        this.introduction = introduction;
        this.headimg = headimg;
        this.parentBlock = parentBlock;
    }

    public void setId(Long id){this.id = id;}
    public void setName(String name){this.name = name;}
    public void setIntroduction(String introduction){this.introduction = introduction;}
    public void setHeadimg(String headimg){this.headimg = headimg;}
    public void setParentBlock(Long parentBlock){this.parentBlock = parentBlock;}

    public Long getId(){return this.id;}
    public String getName(){return this.name;}
    public String getIntroduction(){return this.introduction;}
    public String getHeadimg(){return this.headimg;}
    public Long getParentBlock(){return this.parentBlock;}
}
