package forum.entity;

import javax.persistence.*;

@Entity
@Table(name = "admin")
public class Admin {
    @Id
    @Column(name = "id")
    private Long id;

    public void setId(Long userId){this.id = id;}
    public Long getId(){return this.id;}
}
