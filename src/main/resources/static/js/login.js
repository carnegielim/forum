function LoginCheckName() { //检查名称是否合法
    var username = inputForm.username.value;
    var username_input_div = document.getElementById("username_input_div");
    var check_username_result = document.getElementById("check_username_result");
    var username_sign = document.getElementById("username_sign");
    if(username.length<1 || username.length>9){
        username_input_div.className = "form-group has-error has-feedback no_bottom";
        check_username_result.innerHTML = "请输入长度为1~10的名字";
        username_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        username_input_div.className = "form-group has-success has-feedback no_bottom";
        username_sign.className = "glyphicon glyphicon-ok form-control-feedback";
        check_username_result.innerHTML = "";
    }
    this.OkToLogin();
}
function LoginCheckPassword(){  //检查密码是否合法
    var password = inputForm.password.value;
    var password_input_div = document.getElementById("password_input_div");
    var check_password_result = document.getElementById("check_password_result");
    var password_sign = document.getElementById("password_sign");
    var submit_button = document.getElementById("submit_button");
    if(password.length < 6){
        password_input_div.className = "form-group has-error has-feedback no_bottom";
        check_password_result.innerHTML = "密码需要大于6位！";
        password_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        password_input_div.className = "form-group has-success has-feedback no_bottom";
        password_sign.className = "glyphicon glyphicon-ok form-control-feedback";
        check_password_result.innerHTML = "";
    }
    this.OkToLogin();
}
function RegisterCheckName() { //检查名称是否合法
    var username = inputForm.username.value;
    var username_input_div = document.getElementById("username_input_div");
    var check_username_result = document.getElementById("check_username_result");
    var username_sign = document.getElementById("username_sign");
    if(username.length<1 || username.length>9){
        username_input_div.className = "form-group has-error has-feedback no_bottom";
        check_username_result.innerHTML = "请输入长度为1~10的名字";
        username_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        username_input_div.className = "form-group has-success has-feedback no_bottom";
        username_sign.className = "glyphicon glyphicon-ok form-control-feedback";
        check_username_result.innerHTML = "";
    }
    this.OkToRegister();
}
function RegisterCheckPassword(){  //检查密码是否合法
    var password = inputForm.password.value;
    var password_input_div = document.getElementById("password_input_div");
    var check_password_result = document.getElementById("check_password_result");
    var password_sign = document.getElementById("password_sign");
    if(password.length < 6){
        password_input_div.className = "form-group has-error has-feedback no_bottom";
        check_password_result.innerHTML = "密码需要大于6位！";
        password_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        password_input_div.className = "form-group has-success has-feedback no_bottom";
        password_sign.className = "glyphicon glyphicon-ok form-control-feedback";
        check_password_result.innerHTML = "";
    }
    this.OkToRegister();
}
function ConfirmPassword() {
    var password = inputForm.password.value;
    var password_confirm = inputForm.password_confirm.value;
    var password_confirm_div = document.getElementById("password_confirm_div");
    var check_confirm_result = document.getElementById("check_confirm_result");
    var password_confirm_sign = document.getElementById("password_confirm_sign");
    if (password == password_confirm && password.length >= 6){
        password_confirm_div.className = "form-group has-success has-feedback";
        password_confirm_sign.className = "glyphicon glyphicon-ok form-control-feedback";
        check_confirm_result.innerHTML = "";
    }else if(password != password_confirm){
        password_confirm_div.className = "form-group has-error has-feedback";
        check_confirm_result.innerHTML = "两次输入密码不一致";
        password_confirm_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        password_confirm_div.className = "form-group has-error has-feedback";
        check_confirm_result.innerHTML = "密码需要大于6位！";
        password_confirm_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }
    this.OkToRegister();
}
function JumpToRegister() {
    window.location.href = "register.html";
}
function JumpToLogin() {
    window.location.href = "login";
}
function OkToRegister() {
    var password_confirm_div = document.getElementById("password_confirm_div");
    var password_input_div = document.getElementById("password_input_div");
    var username_input_div = document.getElementById("username_input_div");
    var submit_button = document.getElementById("submit_button");
    if(password_confirm_div.className == "form-group has-success has-feedback"
    && password_input_div.className == "form-group has-success has-feedback no_bottom"
    && username_input_div.className == "form-group has-success has-feedback no_bottom"){
        submit_button.disabled = false;
    }else{
        submit_button.disabled = true;
    }
}
function OkToLogin() {
    var password_input_div = document.getElementById("password_input_div");
    var username_input_div = document.getElementById("username_input_div");
    var submit_button = document.getElementById("submit_button");
    if(password_input_div.className == "form-group has-success has-feedback no_bottom"
        && username_input_div.className == "form-group has-success has-feedback no_bottom"){
        submit_button.disabled = false;
    }else{
        submit_button.disabled = true;
    }
}
function ToRegister() {
    inputForm.submit();
}
function PopWindow(data){
    var button = document.getElementById(data);
    button.click();
}