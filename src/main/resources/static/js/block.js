function getPost(id,index) {

    $.ajax({
        type: "GET",
        url: "/page/post",
        data: {block:id, pageIndex:index},
        dataType: "json",
        success: function(data){
            $('#content').empty();
            for(var i in data){
                $('#content').append(post(data[i].name,data[i].last,data[i].id,data[i].short,data[i].author));
            }
        }
    });
}

function post(name,time,id,short,author) {
    var block = $("<div class=\"col-md-8 col-md-offset-1\"></div>");

    block.append("            <div class=\"box box-info\">\n" +
        "                <div class=\"box-header with-border\">\n" +
        "                    <div class=\"box-title\">"+name+"</div>\n" +
        "                    <div class=\"box-tools pull-right\">\n" +
        "                        <button type=\"button\" class=\"btn btn-box-tool\" data-widget=\"collapse\" data-toggle=\"tooltip\" title=\"\" data-original-title=\"Collapse\">\n" +
        "                            <i class=\"fa fa-minus\"></i></button>\n" +
        "                        <button type=\"button\" class=\"btn btn-box-tool\" onclick=\"jump("+id+")\" data-toggle=\"tooltip\" title=\"\" data-original-title=\"Enter\">\n" +
        "                            <i class=\"fa fa-arrow-right\"></i></button>\n" +
        "                    </div>\n" +
        "                </div>\n" +
        "                <div class=\"box-body\">"+short+"</div>\n" +
        "                <!-- /.box-body -->\n" +
        "                <div class=\"box-footer\">\n" +
        "                    <div class=\"time pull-right\">"+author+"/"+time+"</div>\n" +
        "                </div>\n" +
        "            </div>");
    return block;
}
function jump(id) {
    window.location.href = "/post?id="+id;
}