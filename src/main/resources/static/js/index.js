function getChart(data) {

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            datasets: [{
                label: "注册人数",
                backgroundColor: 'rgb(131, 228, 227)',
                borderColor: 'rgb(255, 99, 132)',
                data: [data[0], data[1],data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11]],
            }]
        },

        // Configuration options go here
        options: {}
    });
}
