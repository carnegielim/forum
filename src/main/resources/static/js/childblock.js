function getBlock(id,index) {
    $.ajax({
        type: "GET",
        url: "/page/block",
        data: {block:id, pageIndex:index},
        dataType: "json",
        success: function(data){
            $('#content').empty();
            for(var i in data){
                $('#content').append(block(data[i].name,data[i].introduction,data[i].id));

            }
        }
    });
}

function block(name,introduction,id) {
    var block = $("<div></div>");

    block.append("<div class=\"col-md-8 col-md-offset-1 row\">" +
        "            <div class=\"box box-primary\">" +
        "                <div class=\"box-header with-border\">" +
        "                    <h3 class=\"box-title\">"+name+"</h3>" +
        "                    <div class=\"box-tools pull-right\">" +
        "                        <button type=\"button\" class=\"btn btn-box-tool\" data-widget=\"collapse\" data-toggle=\"tooltip\" title=\"\" data-original-title=\"Collapse\">" +
        "                            <i class=\"fa fa-minus\"></i></button>" +
        "                        <button type=\"button\" class=\"btn btn-box-tool\" data-widget=\"remove\" data-toggle=\"tooltip\" title=\"\" data-original-title=\"Remove\">" +
        "                            <i class=\"fa fa-times\"></i></button>" +
        "                    </div>" +
        "                </div>" +
        "                <div class=\"box-body\">"+introduction+"</div>" +
        "                <!-- /.box-body -->" +
        "                <div class=\"box-footer\">" +
        "                    <a class=\"btn btn-primary\" href=\"/allblock/block?block="+id+"\">进入板块</a>" +
        "                </div>" +
        "                <!-- /.box-footer-->" +
        "            </div>" +
        "        </div>");
    return block;
}