function tosubmit() {//留言前检查留言正确性
    this.checkcontent();
    var check_content_result = document.getElementById("check_content_result").innerHTML;
    if(check_content_result == "输入成功"){
        inputForm.submit();
    }
}
function checkname() { //检查名称是否合法
    var name = inputForm.name.value;
    var divname = document.getElementById("divname");
    var check_name_result = document.getElementById("check_name_result");
    var name_sign = document.getElementById("name_sign");
    if(name.length<1 || name.length>9){
        divname.className = "form-group has-error has-feedback";
        check_name_result.innerHTML = "请输入长度为1~10的名字";
        name_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        check_name_result.innerHTML = "输入成功";
        divname.className = "form-group has-success has-feedback";
        name_sign.className = "glyphicon glyphicon-ok form-control-feedback";
    }
}
function checkemail(){  //检查邮箱是否合法
    var email = inputForm.email.value;
    var divemail = document.getElementById("divemail");
    var check_email_result = document.getElementById("check_email_result");
    var email_sign = document.getElementById("email_sign");
    var myReg=/^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;
    if(!myReg.test(email)){
        divemail.className = "form-group has-error has-feedback";
        check_email_result.innerHTML = "请输入正确的邮箱";
        email_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        check_email_result.innerHTML = "输入成功";
        divemail.className = "form-group has-success has-feedback";
        email_sign.className = "glyphicon glyphicon-ok form-control-feedback";
    }
}
function checkcontent(){    //检查内容是否合法
    var content = inputForm.content.value;
    var divcontent = document.getElementById("divcontent");
    var check_content_result = document.getElementById("check_content_result");
    var content_sign = document.getElementById("content_sign");
    if(content.length<1){
        divcontent.className = "form-group has-error has-feedback";
        check_content_result.innerHTML = "留言内容不能为空！";
        content_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else if(content.length>255){
        divcontent.className = "form-group has-error has-feedback";
        check_content_result.innerHTML = "留言内容不能超过255字！";
        content_sign.className = "glyphicon glyphicon-remove form-control-feedback"
    }else{
        check_content_result.innerHTML = "输入成功";
        divcontent.className = "form-group has-success has-feedback";
        content_sign.className = "glyphicon glyphicon-ok form-control-feedback";
    }
}
function todel(data1){  //创建虚拟表单后请求del.jsp，删除留言
    var tempForm = document.createElement("form");
    tempForm.id = "tempForm1";
    tempForm.method = "post";
    tempForm.action = "del.jsp";
    var hideInput1 = document.createElement("input");
    hideInput1.type = "hidden";
    hideInput1.name="rank";
    hideInput1.value = data1;
    tempForm.appendChild(hideInput1);
    //将tempForm 表单添加到 documentbody之后
    document.body.appendChild(tempForm);
    tempForm.submit();
    document.body.removeChild(tempForm);

}
function todel_response(data1){  //创建虚拟表单后请求delresponse.jsp,删除回复
    var tempForm = document.createElement("form");
    tempForm.id = "tempForm1";
    tempForm.method = "post";
    tempForm.action = "delresponse.jsp";
    var hideInput1 = document.createElement("input");
    hideInput1.type = "hidden";
    hideInput1.name="rank";
    hideInput1.value = data1;
    tempForm.appendChild(hideInput1);
    document.body.appendChild(tempForm);
    tempForm.submit();
    document.body.removeChild(tempForm);
}
function toresponse(data){  //弹出回复框
    var response = document.getElementById("response"+data);
    var del = document.getElementById("delete"+data);
    var confirm = document.getElementById("confirm"+data);
    var retn = document.getElementById("return"+data);
    response.className += " disappear";
    del.className += " disappear";
    confirm.className = "btn btn-primary pull-right";
    retn.className = "btn btn-info pull-right";
    $("#responseinputdiv"+data).animate({height: 'toggle', opacity: 'toggle'}, "slow");

}
function toreturn(data) {   //返回，即收回回复框
    var response = document.getElementById("response"+data);
    var del = document.getElementById("delete"+data);
    var confirm = document.getElementById("confirm"+data);
    var retn = document.getElementById("return"+data);
    response.className = "btn btn-info pull-right";
    del.className = "btn btn-danger pull-right";
    confirm.className += " disappear";
    retn.className += " disappear";
    $("#responseinputdiv"+data).animate({height: 'toggle', opacity: 'toggle'}, "slow");
}
function hide_response(data) { //折叠回复
    $("#responsediv"+data).animate({height: 'toggle', opacity: 'toggle'}, "slow");
    var hide = document.getElementById("hide_response"+data);
    hide.className += " disappear";
    var show = document.getElementById("show_response"+data);
    show.className = "btn btn-info pull-right";
}
function show_response(data) { //折叠回复
    $("#responsediv"+data).animate({height: 'toggle', opacity: 'toggle'}, "slow");
    var hide = document.getElementById("hide_response"+data);
    var show = document.getElementById("show_response"+data);
    show.className += " disappear";
    hide.className = "btn btn-primary pull-right";
}
function responseSubmit(data){
    var responseForm = document.getElementById("responseForm"+data);
    responseForm.submit();
}
function open_response(data){
    $("#responsediv"+data).animate({height: 'toggle', opacity: 'toggle'}, "slow");
    var response_open = document.getElementById("response_open"+data);
    var response_close = document.getElementById("response_close"+data);
    response_open.className += " disappear";
    response_close.className = "time col-lg-2 col-lg-offset-1";
}
function close_response(data){
    $("#responsediv"+data).animate({height: 'toggle', opacity: 'toggle'}, "slow");
    var response_open = document.getElementById("response_open"+data);
    var response_close = document.getElementById("response_close"+data);
    response_close.className += " disappear";
    response_open.className = "time col-lg-2 col-lg-offset-1";
}

function getResponse(id,index,isAdmin) {

    $.ajax({
        type: "GET",
        url: "/page/response",
        data: {post:id, pageIndex:index},
        dataType: "json",
        success: function(data){
            $('#content').empty();
            for(var i in data){
                $('#content').append(response(data[i].id,data[i].author,data[i].content,data[i].time,data[i].rank,isAdmin));
            }
        }
    });
}
function response(id,author,content,time,rank,isAdmin) {
    var adminOp;
    if(isAdmin == true){
        if(rank == 1){
            adminOp = "<a class=\"col-lg-3\" href='/admin/delpostbyresponse?id="+id+"'>删除</a>\n";
        }else{
            adminOp = "<a class=\"col-lg-3\" href='/admin/delresponse?id="+id+"'>删除</a>\n";
        }
    }else
        adminOp = "<div class=\"col-lg-3\"></div>\n"


    var block = $("<div class=\"panel panel-info col-lg-8 col-lg-offset-2\"></div>");

    block.append("    <div class=\"row myrow\">\n" +
        "    <div class=\"col-lg-2 bg-info\">\n" +
        "    <br>\n" +
        "    <img  class=\"img-responsive img-circle\" src=\"/headimg/nohead.png \">\n" +
        "    <p class=\"text-center\">"+author+"</p>\n" +
        "    </div>\n" +
        "    <div class=\"col-lg-10\">\n" +
        "    <p class=\"message autoenter\">"+content+"</p>\n" +
        "    </div>\n" +
        "    </div>\n" +
        "    <!--留言提示信息-->\n" +
        "    <div class=\"row\">\n" +
        "    <div class=\"bg-info col-lg-2\"><br></div>\n" +
         adminOp +
        "    <div class=\"col-lg-7 text-right text-muted time\">\n" +
        rank+"楼/"+time+"\n" +
        "    </div>\n" +
        "    </div>\n" +
        "    </div>\n" +
        "    </div>");
    return block;
}
